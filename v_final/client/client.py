import socket
import sys
import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.uic import loadUi
import matplotlib.pyplot as plt
from cryptography.fernet import Fernet

class Client(QDialog):
	def __init__(self):
		super(Client, self).__init__()
		loadUi('client.ui', self)
		self.buttonTrigger()
		self.axes = plt.gca()
		self.axes.set_ylim([-2.1,2.1])
		self.criptKey=b'bo3drB8LZvlhJuXBD2AGMX1_cvKJoM6Our44Otn0tH0='
	def buttonTrigger(self):
		self.sendButton.clicked.connect(self.sendMsg)
		self.selectBox.currentTextChanged.connect(self.update_selectBox)

	def sendMsg(self):
		self.wave=[0]
		host = '127.0.0.1'  
		port = 5015
		address=(host,port)

		sock = socket.socket()
		sock.connect(address)

		self.msg=self.originText.toPlainText()
		
		self.codificaMsg()
		self.update()

		sock.send(self.cod.encode('utf8'))
		plt.show()
		sock.close()
		
	def codificaMsg(self):
		self.msg2cript()
		self.cript2bin()
		self.bin2cod()

	def msg2cript(self):
		key = self.criptKey
		f = Fernet(key)
		self.cript=f.encrypt(self.msg.encode("utf8"))
		
	def cript2bin(self):
		self.bin=self.toBinary(self.cript.decode("utf8"))
		
	def bin2cod(self):
		self.cod=""
		i=0
		j=0
		while i in range(len(self.bin)):
			aux=self.bin[i]+self.bin[i+1]
			if(aux=="00"):
				self.cod+="-2"
				self.wave.append(-2)
			if(aux=="01"):
				self.cod+="+1"
				self.wave.append(+1)
			if(aux=="10"):
				self.cod+="-1"
				self.wave.append(-1)
			if(aux=="11"):
				self.cod+="+2"
				self.wave.append(+2)
			i+=2

		self.wave.append(0)
	def toBinary(self,string):
		return "".join([format(ord(char),'#010b')[2:] for char in string])

	def update_selectBox(self):
		if(self.selectBox.currentText() == "Criptografia"):
			self.selectText.setText(self.cript.decode("utf8"))
		
		elif(self.selectBox.currentText() == "Binario"):
			self.selectText.setText(self.bin)
		
		elif(self.selectBox.currentText() == "Código de linha"):
			self.selectText.setText(self.cod)
	
	def update_graf(self):
		plt.plot(self.wave, color='red', drawstyle='steps')
		plt.title('Gráfico de onda - Enviada')
		plt.ylabel('Amplitude')
		plt.grid(True, which='both')
		plt.axhline(y=0, color='k')
		plt.ylim(-2.1, 2.1)
		plt.savefig('wave.png')
		self.wave_form.setPixmap(QtGui.QPixmap("wave.png"))

	def update(self):
		self.update_selectBox()
		self.update_graf()

app = QApplication(sys.argv)
widget = Client()
widget.show()
sys.exit(app.exec_())
