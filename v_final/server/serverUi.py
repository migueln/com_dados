# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'server.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(342, 581)
        Dialog.setMinimumSize(QtCore.QSize(342, 581))
        Dialog.setMaximumSize(QtCore.QSize(342, 581))
        self.connectButton = QtWidgets.QPushButton(Dialog)
        self.connectButton.setGeometry(QtCore.QRect(10, 120, 75, 21))
        self.connectButton.setAutoDefault(False)
        self.connectButton.setDefault(False)
        self.connectButton.setFlat(False)
        self.connectButton.setObjectName("connectButton")
        self.originText = QtWidgets.QTextEdit(Dialog)
        self.originText.setGeometry(QtCore.QRect(10, 30, 321, 91))
        self.originText.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.originText.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.originText.setObjectName("originText")
        self.selectText = QtWidgets.QTextEdit(Dialog)
        self.selectText.setGeometry(QtCore.QRect(10, 200, 321, 81))
        self.selectText.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.selectText.setObjectName("selectText")
        self.selectBox = QtWidgets.QComboBox(Dialog)
        self.selectBox.setGeometry(QtCore.QRect(10, 280, 111, 22))
        self.selectBox.setFrame(True)
        self.selectBox.setObjectName("selectBox")
        self.selectBox.addItem("")
        self.selectBox.addItem("")
        self.selectBox.addItem("")
        self.label_onda = QtWidgets.QLabel(Dialog)
        self.label_onda.setGeometry(QtCore.QRect(10, 390, 81, 21))
        self.label_onda.setFrameShape(QtWidgets.QFrame.Box)
        self.label_onda.setObjectName("label_onda")
        self.wave_form = QtWidgets.QLabel(Dialog)
        self.wave_form.setGeometry(QtCore.QRect(10, 410, 321, 161))
        self.wave_form.setFrameShape(QtWidgets.QFrame.Panel)
        self.wave_form.setText("")
        self.wave_form.setPixmap(QtGui.QPixmap("../client/versao-proto/wave.jpg"))
        self.wave_form.setScaledContents(True)
        self.wave_form.setObjectName("wave_form")
        self.originText.raise_()
        self.selectText.raise_()
        self.selectBox.raise_()
        self.label_onda.raise_()
        self.wave_form.raise_()
        self.connectButton.raise_()

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.connectButton.setText(_translate("Dialog", "Conectar"))
        self.selectBox.setItemText(0, _translate("Dialog", "Criptografia"))
        self.selectBox.setItemText(1, _translate("Dialog", "Binario"))
        self.selectBox.setItemText(2, _translate("Dialog", "Código de linha"))
        self.label_onda.setText(_translate("Dialog", "Forma de onda"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
