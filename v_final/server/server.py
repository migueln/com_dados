import socket
import sys
import time
import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.uic import loadUi
import matplotlib.pyplot as plt
from cryptography.fernet import Fernet


class Server(QDialog):
	def __init__(self):
		super(Server, self).__init__()
		loadUi('server.ui', self)
		self.buttonTrigger()
		self.axes = plt.gca()
		self.axes.set_ylim([-2.1,2.1])
		self.criptKey=b'bo3drB8LZvlhJuXBD2AGMX1_cvKJoM6Our44Otn0tH0='

	def buttonTrigger(self):
		self.connectButton.clicked.connect(self.receive)
		self.selectBox.currentTextChanged.connect(self.update_selectBox)


	### THE CODE

	def receive(self):
		self.wave=[0]

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		host = '127.0.0.1'  
		port = 5015

		s.bind((host, port))
		s.listen(5)


		client_socket, address = s.accept()
		print("Conencted to - ", address, "\n")
		data = client_socket.recv(2048)
		self.cod = data.decode('utf8')
		self.decodificaMsg()

		self.update()
		print(self.msg)

		self.originText.setText(self.msg)
		plt.show()
		s.close()

	def decodificaMsg(self):
		self.cod2bin()
		self.bin2cript()
		self.cript2msg()

	def cod2bin(self):
		self.bin=""
		i=0
		while i in range(len(self.cod)):
			aux=self.cod[i]+self.cod[i+1]
			if(aux=="-2"):
				self.wave.append(-2)
				self.bin+="00"
			if(aux=="+1"):
				self.wave.append(+1)
				self.bin+="01"
			if(aux=="-1"):
				self.wave.append(-1)
				self.bin+="10"
			if(aux=="+2"):
				self.wave.append(+2)
				self.bin+="11"
			i+=2
		self.wave.append(0)

	def bin2cript(self):
		print(bin)
		self.cript=self.toString(self.bin)
		self.cript=self.cript.encode("utf8")
	def cript2msg(self):
		key = self.criptKey
		f = Fernet(key)

		self.msg=f.decrypt(self.cript).decode("utf8")	

	def toString(self,binaryString):
		return "".join([chr(int(binaryString[i:i+8],2)) for i in range(0,len(binaryString),8)])


	def update_selectBox(self):
		if(self.selectBox.currentText() == "Criptografia"):
			self.selectText.setText(self.cript.decode("utf8"))
		
		elif(self.selectBox.currentText() == "Binario"):
			self.selectText.setText(self.bin)
		
		elif(self.selectBox.currentText() == "Código de linha"):
			self.selectText.setText(self.cod)
		
	def update_graf(self):
		f=plt.figure(1)
		plt.plot(self.wave, color='red', drawstyle='steps')
		plt.title('Gráfico de onda - Recebida')
		plt.ylabel('Amplitude')
		plt.grid(True, which='both')
		plt.axhline(y=0, color='k')
		plt.ylim(-2.1, 2.1)
		plt.savefig('wave.png')
		self.wave_form.setPixmap(QtGui.QPixmap("wave.png"))
		
	def update(self):
		self.update_selectBox()
		self.update_graf()

app = QApplication(sys.argv)
widget = Server()
widget.show()
sys.exit(app.exec_())







